#ifndef RUN_HPP
#define RUN_HPP

#include <iostream>

namespace run {
    template <typename T>
    void print(T mesg);
}

#include "run.ipp"

#endif //RUN_HPP
