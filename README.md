# cpp-template

An extremely contrived "Hello world" program in C++ to remind me how to set up a C++ project and compile it.
I've chosen the `.cpp`/`.hpp`/`.ipp` convention for source code, used `.o` for object files, and no suffix for executables.

## Project source code layout

* All source code **declarations** in `.hpp` (header)
* **Implementation** of declared _template functions_ in `.ipp` (inline)
* All other _function_ **implementations** and `main` code in `.cpp` (C++)
* `Makefile` defines compilation with `make` 

## Description

A _function_ is **implemented** in `func.cpp` with **declaration** in `func.hpp`.
A _template function_ is **implemented** in `run.ipp` with **declaration** in `run.hpp`.
A _namespace_ is defined for each header, and `if` statements ensure headers are included only once.
Both _functions_ are used in `run.cpp`, which contains the `main` source for the executable `run`.
Compile with `make run`, or just `make`.

## Useful resources

* [Compiling versus linking](https://stackoverflow.com/a/6264256)
* [cppreference](https://en.cppreference.com/w/)
* [GNU make docs](https://www.gnu.org/software/make/manual/html_node/index.html#SEC_Contents)
